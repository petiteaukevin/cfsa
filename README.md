# cfsa
# outils application
* WampServeur
* VisualStudioCode

# Synchro application
* php bin/console doctrine:database:create : Permets de créer la base de données depuis les sources PHP
* php bin/console doctrine:schema:update --force : Mets à jour la base depuis les sources PHP

# start application
cd ../cfsa
symfony server:start
