<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Materiel;
use App\Entity\Locataire;
use App\Entity\Reservation;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
       
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(MaterielCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Cfsa');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Accueil', 'fa fa-home');
        yield MenuItem::linkToCrud('Matériel', 'fas fa-dumbbell', Materiel::class);
        yield MenuItem::linkToCrud('Client', 'fas fa-user', Locataire::class);
        yield MenuItem::linkToCrud('Réservation', 'fas fa-user', Reservation::class);
        // yield MenuItem::linkToCrud('The Label', 'icon class', EntityClass::class);
        //yield MenuItem::linkToCrud('The Label', 'fas fa-th-list', Materiel::class);
        //yield MenuItem::linkToCrud('menu.product.list', 'fas fa-th-list', Materiel::class)->setDefaultSort(['createdAt' => 'DESC']);
    }
}
