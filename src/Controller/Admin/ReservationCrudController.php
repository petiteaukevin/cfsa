<?php

namespace App\Controller\Admin;

use App\Entity\Locataire;
use App\Entity\Reservation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Psr\Log\LoggerInterface;

class ReservationCrudController extends AbstractCrudController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getEntityFqcn(): string
    {
        return Reservation::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            DateField::new('dateDebut'),
            DateField::new('dateFin'),
            TextField::new('commentaire'),
            NumberField::new('quantite'),
            AssociationField::new('client'),
            AssociationField::new('materiel'),
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        $reservation = new Reservation();
        

        return $reservation;
    }
    
    
}
