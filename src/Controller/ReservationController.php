<?php

namespace App\Controller;

use App\Entity\Materiel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ReservationController extends AbstractController
{

    /**
     * @Route("/reservation", name="app_reservation")
     */
    public function main(Request $request): Response
    {
        $data = array(
            'value' => null,
            'number' => 10,
            'string' => 'No value',
        );

        $form = $this->createFormBuilder()
                 ->add('Email', EmailType::class, array('required' => true))
                 ->add('Nom', TextType::class, array('required' => false))
                 ->add('Prenom', TextType::class, array('required' => false))
                 ->add('Association', TextType::class, array('required' => false))
                 ->add('DateDebutDeReservation', DateType::class, [
                    // renders it as a single text box
                    'widget' => 'single_text',
                    'required' => true
                    ])
                 ->add('DateFinDeReservation', DateType::class, [
                    // renders it as a single text box
                    'widget' => 'single_text',
                    'required' => true
                    ])   
                 ->add('AssiettesBlanchesGrande', IntegerType::class, [
                    'attr' => [
                        'min' => 5,
                        'max' => 50
                    ],
                    'required' => false
                ])
                 ->add('save', SubmitType::class)
                 ->getForm();

        $form2 = $this->createFormBuilder()->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
                
                $listMateriel = $this->getDoctrine()->getRepository(Materiel::class)->findAll();
                $form2 = $this->createFormBuilder($listMateriel);
                foreach ( $listMateriel as $materiel ){
                    $form2->add('materiel'.$materiel->getId(), IntegerType::class, [
                        'attr' => [
                            'min' => 5,
                            'max' => 50,
                            'libelle' => $materiel->getDesignation(),
                            'tarif' => 'test'
                        ],
                        'required' => false
                    ]);
                }
                $form2 =  $form2->getForm();
                return $this->render('reservation/reservation.html.twig' , array(
                    'form' => $form->createView(),
                    'form2' => $form2->createView(),
                ));
            }
        }

                


        return $this->render('reservation/reservation.html.twig' , array(
            'form' => $form->createView(),
            'form2' => $form2->createView(),
        ));
    }
}
