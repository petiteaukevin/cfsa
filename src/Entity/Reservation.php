<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valider;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $commentaire;
    
    /**
     * @ORM\ManyToOne(targetEntity="Locataire", inversedBy="reservations")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="Materiel", inversedBy="reservations")
     */
    private $materiel;

    /**
     * @ORM\Column(type="integer")
     */ 
    private $quantite;

    public function __construct()
    {
       
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getclient()
    {
        return $this->client;
    }

    public function setClient(?Locataire $locataire): self
    {
        $this->client = $locataire;

        return $this;
    }

    public function getMateriel()
    {
        return $this->materiel;
    }

    public function setMateriel(?Materiel $materiel): self
    {
        $this->materiel = $materiel;

        return $this;
    }

    public function isValider(){
        return $this->valider;
    }

    public function setValider(?Boolean $valider): self
    {
        $this->valider = $valider;

        return $this;
    }

    public function getCommentaire(){
        return $this->commentaire;
    }

    public function setCommentaire(?String $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
}
